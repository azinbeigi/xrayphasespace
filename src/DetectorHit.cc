
#include "DetectorHit.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4VisAttributes.hh"

G4Allocator<DetectorHit>DetectorHitAllocator;

DetectorHit::DetectorHit()
{;}

DetectorHit::~DetectorHit()
{
  G4cout<<"~DetectorHit()  "<<G4endl;
}

DetectorHit::DetectorHit(const DetectorHit &right)
{
  edep = right.edep;
  pos = right.pos;
}

const DetectorHit& DetectorHit::operator=(const DetectorHit &right)
{
  edep = right.edep;
  pos = right.pos;
  return *this;
  
}

int DetectorHit::operator==(const DetectorHit &right) const
{
  return 0;
}

void DetectorHit::Draw()
{
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager)
    {
      G4Circle circle(pos);
      circle.SetScreenSize(0.04);
      circle.SetFillStyle(G4Circle::filled);
      G4Colour colour(1.,0.,0.);
      G4VisAttributes attribs(colour);
      circle.SetVisAttributes(attribs);
      pVVisManager->Draw(circle);
    }
  
}

void DetectorHit::Print()
{;}


// This is a forward declarations of an instantiated G4Allocator<Type> object.
// It has been added in order to make code portable for the GNU g++ 
// (release 2.7.2) compiler. 
// Whenever a new Type is instantiated via G4Allocator, it has to be forward
// declared to make object code (compiled with GNU g++) link successfully. 
// 
#ifdef GNU_GCC
  template class G4Allocator<DetectorHit>;
#endif










