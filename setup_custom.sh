#!/bin/bash

#Load modules and source Geant4 and Root setup scripts
ml nixpkgs/16.09 gcc/7.3.0 root/6.14.04 xerces-c++/3.2.2
source /cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/gcc7.3/root/6.14.04/bin/thisroot.sh
source $HOME/projects/def-senger/CommonGroup/Geant4/geant4.10.02.p02_install/share/Geant4-10.2.2/geant4make/geant4make.sh

#Establish paths to BrachySource source library and executable
export BSOURCE_DIR=$HOME/projects/def-senger/CommonGroup/RapidBrachyMC
export BSOURCE_EXEC=$HOME/projects/def-senger/CommonGroup/Executable/bin/Linux-g++/BrachySource


#Establish paths to python submit scripts
export SENDJOB_PY=$HOME/projects/def-senger/CommonGroup/SubmitScripts/sendJob.py
export SENDMISSINGJOBS_PY=$HOME/projects/def-senger/CommonGroup/SubmitScripts/sendMissingJobs.py


#Can change path to build compiled G4 executables by providing argument
if [ $# -eq 1 ]
then
	export G4WORKDIR=$1
	#alias BrachySource=	
else
	export G4WORKDIR=$HOME/geant4_workdir
fi

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/geant4_workdir/tmp/Linux-g++:$HOME/projects/def-senger/CommonGroup/Executable:$HOME/projects/def-senger/CommonGroup/Executable/tmp/Linux-g++/BrachySource/:/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/gcc7.3/xerces-c++/3.2.2/lib

echo "If using Geant4 with GDML, please make sure to add the following lines to your GNUmakefile: 

CPPFLAGS += -L/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/gcc7.3/xerces-c++/3.2.2/lib

LIBS += libxerces-c.so -lxerces-c"
