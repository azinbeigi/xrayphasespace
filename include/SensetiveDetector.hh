
#ifndef SensetiveDetector_h
#define SensetiveDetector_h 1

#include "G4VSensitiveDetector.hh"
#include "G4ThreeVector.hh"
#include "DetectorHit.hh"
#include "G4Step.hh"
#include "G4HCofThisEvent.hh"

class SensetiveDetector: public G4VSensitiveDetector
{
public:
  SensetiveDetector(G4String);
  ~SensetiveDetector();
  void initialize(G4HCofThisEvent* HCE);
  G4bool ProcessHits(G4Step* aStep, G4TouchableHistory* ROhist);
  void EndOfEvent(G4HCofThisEvent* HCE);
  void clear();
  void DrawAll();
  void PrintAll();
private:
  DetectorHitsCollection* Collection;
};
#endif
