#ifndef SteppingAction_h
#define SteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "EventAction.hh"
#include "G4Step.hh"
#include "G4Track.hh"
#include "G4ParticleChange.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

class SteppingAction : public G4UserSteppingAction
{
  public:
    SteppingAction(EventAction*);
    virtual ~SteppingAction();

    virtual void UserSteppingAction(const G4Step*);
     

  private:
    EventAction* eventAction;
    G4String ProcessN;
};

#endif
