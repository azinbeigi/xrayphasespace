#ifndef RunAction_h
#define RunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "DetectorConstruction.hh"


#include "TH1.h"
#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"


class G4Run;

class RunAction : public G4UserRunAction
{
  public:
  RunAction(DetectorConstruction* ,long int);

   ~RunAction();

  public:
     void BeginOfRunAction(const G4Run*);
     void EndOfRunAction(const G4Run*);


     void FillHisto_X_Rays_Target(G4double);
     void FillHisto_X_Rays_Filter1(G4double);
     void FillHisto_X_Rays_Filter2(G4double);
     void FillHisto_X_Rays_Filter3(G4double);
     void FillHistoTotal(G4ThreeVector,G4double);
     void FillHistoPhaseSpace(G4ThreeVector, G4ThreeVector,G4double, G4double);
     void FillHistoPhaseSpace2(G4ThreeVector, G4ThreeVector,G4double, G4double);
private:
  DetectorConstruction* detector;



  G4int runIDcounter;
  long int MyTime;

  G4String fileName;


  TFile* histoFile;
  TH1F *X_Rays_Target,*X_Rays_Filter1,*X_Rays_Filter2, *X_Rays_Filter3;
  TTree *Total_Deposit, *PhaseSp, *PhaseSp2;
  G4double xTotal,yTotal,zTotal,ETotal,xPh,yPh,zPh, EPh, xmPh,ymPh, zmPh;
  G4double xPh2,yPh2,zPh2, EPh2, xmPh2,ymPh2, zmPh2,wPh, wPh2;


};

#endif
