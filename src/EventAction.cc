#include "G4ios.hh"
#include "EventAction.hh"
#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4VVisManager.hh"
#include "G4UImanager.hh"
#include "G4UnitsTable.hh"

#include "TH1.h"
#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"

extern G4bool drawEvent;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
EventAction::EventAction(RunAction* aRun):Run(aRun),drawFlag("all")
{
  dose=0.0;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

EventAction::~EventAction()
{
  ;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void EventAction::BeginOfEventAction(const G4Event* )
{
  ;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void EventAction::EndOfEventAction(const G4Event* evt)
{

  G4int EvtNb = evt -> GetEventID();
  if(EvtNb%50000==0)
    {


      G4cout<<">>>>Event nr:"<<EvtNb<<G4endl;
      G4cout<<">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<G4endl;
      //G4cout<<"Total dose (Gy) deposited in the breast: "<<GetTheDoseInTheBreast()/CLHEP::gray<<G4endl;


    }

}


/*void EventAction::Set_X_Rays_Target(G4double energy)
{

  TargetXE=energy;
  Run ->FillHisto_X_Rays_Target(TargetXE);
}*/

/*void EventAction::Set_X_Rays_Filter1(G4double energy)
{

  Filter1XE=energy;
  Run ->FillHisto_X_Rays_Filter1(Filter1XE);
}*/

void EventAction::Set_X_Rays_Filter2(G4double energy)
{

  Filter2XE=energy;
  Run ->FillHisto_X_Rays_Filter2(Filter2XE);
}

/*void EventAction::Set_X_Rays_Filter3(G4double energy)
{
  Filter3XE=energy;
  Run ->FillHisto_X_Rays_Filter3(Filter3XE);
}
/*
void EventAction::SetPos_E_Total(G4ThreeVector position,G4double energy)
{

  posTotal=position;
  TotalEdep=energy;
  Run ->FillHistoTotal(posTotal,TotalEdep);

}


void EventAction::SetTheDoseInTheBreast(G4double D)
{

  dose=D;
}
G4double EventAction::GetTheDoseInTheBreast()

{

  return dose;

}
*/
void EventAction::SetPhaseSpace(G4ThreeVector position, G4ThreeVector momentum, G4double energy, G4double weight)
{

  posPhase=position;
  momPhase=momentum;
  KinPhase=energy;
  wPhase=weight;
  Run ->FillHistoPhaseSpace(posPhase,momPhase,KinPhase,wPhase);
}

/*void EventAction::SetPhaseSpace2(G4ThreeVector position, G4ThreeVector momentum, G4double energy, G4double weight)
{

  posPhase2=position;
  momPhase2=momentum;
  KinPhase2=energy;
  wPhase2=weight;
  Run ->FillHistoPhaseSpace2(posPhase2,momPhase2,KinPhase2, wPhase2);
}*/
