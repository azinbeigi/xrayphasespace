#include "RunAction.hh"


#include "G4Run.hh"
#include "G4UImanager.hh"
#include "G4VVisManager.hh"
#include "G4ios.hh"

#include "CLHEP/Random/Random.h"

#include <cstdlib>
#include <sstream>
#include <iostream>
#include <string>
using namespace std;


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

RunAction::RunAction(DetectorConstruction* MyDetector,long int tid)
  :detector(MyDetector),MyTime(tid)
{
  ;
}
RunAction::~RunAction()
{

}
void RunAction::BeginOfRunAction(const G4Run* aRun)
{
  G4cout << "### Run " << aRun->GetRunID() << " start." <<G4endl;
  if(G4VVisManager::GetConcreteInstance()) {
    G4UImanager* UI = G4UImanager::GetUIpointer();
    UI->ApplyCommand("/vis/scene/notifyHandlers");
  }
  CLHEP::HepRandom::showEngineStatus();

  string ending = ".xRay.root";
  stringstream FileNameStream;
  FileNameStream <<MyTime<<ending;
  G4String Name = FileNameStream.str();
  histoFile = new TFile(Name,"RECREATE","Produced Spectrum Histograms");


 // X_Rays_Target= new TH1F("X_Rays_Target","X-rays produced in target WTi",450, 5, 50);
 // X_Rays_Filter1= new TH1F("X_Rays_Anode","X-rays after anode substrate AlN:Y2O3",450, 5, 50);
  X_Rays_Filter2= new TH1F("X_Rays_Jacket","X-rays at source jacket surface",450, 5, 50);
  //X_Rays_Filter3= new TH1F("X_Rays_Sphere","X-rays at 178 cm from source in air",450, 5, 50);
 
  // X_Rays_Target= new TH1F("X_Rays_Target","X-rays produced in target",2000, 0, 200);
  // X_Rays_Filter1= new TH1F("X_Rays_Filter1","X-rays after the first filter",2000, 0, 200);
  // X_Rays_Filter2= new TH1F("X_Rays_Filter2","X-rays after the second filter",2000, 0, 200);

  PhaseSp = new TTree("PhaseSpace"," PhaseSpace ");
  PhaseSp->Branch("xPh",&xPh,"xPh/D");
  PhaseSp->Branch("yPh",&yPh,"yPh/D");
  PhaseSp->Branch("zPh",&zPh,"zPh/D");
  PhaseSp->Branch("xmPh",&xmPh,"xmPh/D");
  PhaseSp->Branch("ymPh",&ymPh,"ymPh/D");
  PhaseSp->Branch("zmPh",&zmPh,"zmPh/D");
  PhaseSp->Branch("EPh",&EPh,"EPh/D");
  PhaseSp->Branch("wPh",&wPh, "wPh/D");


  /*PhaseSp2 = new TTree("PhaseSpace2"," PhaseSpace2 ");
  PhaseSp2->Branch("xPh",&xPh2,"xPh/D");
  PhaseSp2->Branch("yPh",&yPh2,"yPh/D");
  PhaseSp2->Branch("zPh",&zPh2,"zPh/D");
  PhaseSp2->Branch("xmPh",&xmPh2,"xmPh/D");
  PhaseSp2->Branch("ymPh",&ymPh2,"ymPh/D");
  PhaseSp2->Branch("zmPh",&zmPh2,"zmPh/D");
  PhaseSp2->Branch("EPh",&EPh2,"EPh/D");
  PhaseSp2->Branch("wPh",&wPh2,"wPh/D");*/

  /*
  Total_Deposit = new TTree("TotalEDeposit"," Total energy deposition in Detector");
  Total_Deposit->Branch("xTotal",&xTotal,"xTotal/D");
  Total_Deposit->Branch("yTotal",&yTotal,"yTotal/D");
  Total_Deposit->Branch("zTotal",&zTotal,"zTotal/D");
  Total_Deposit->Branch("ETotal",&ETotal,"ETotal/D");
  */

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void RunAction::EndOfRunAction(const G4Run* )
{

  CLHEP::HepRandom::showEngineStatus();
  //X_Rays_Target->Print();
 // X_Rays_Filter1->Print();
  X_Rays_Filter2->Print();
 // X_Rays_Filter3->Print();
  PhaseSp->Print();
  // Total_Deposit->Print();

  histoFile ->Write();



}


/*void RunAction:: FillHisto_X_Rays_Target(G4double Energy)
{

  X_Rays_Target -> Fill(Energy);
}*/

/*void RunAction:: FillHisto_X_Rays_Filter1(G4double Energy)
{

  X_Rays_Filter1 -> Fill(Energy);
}*/

void RunAction:: FillHisto_X_Rays_Filter2(G4double Energy)
{

  X_Rays_Filter2 -> Fill(Energy);
}

/*void RunAction:: FillHisto_X_Rays_Filter3(G4double Energy)
{

  X_Rays_Filter3 -> Fill(Energy);
}*/
/*
void RunAction:: FillHistoTotal(G4ThreeVector posTotal,G4double EnergyDeposit)
{
  xTotal= posTotal(0);
  yTotal  = posTotal(1);
  zTotal= posTotal(2);
  ETotal=EnergyDeposit;
  Total_Deposit -> Fill();

}
*/
void RunAction:: FillHistoPhaseSpace(G4ThreeVector position, G4ThreeVector momentum,G4double Energy, G4double weight)
{

  xPh= position(0);
  yPh  = position(1);
  zPh= position(2);
  xmPh= momentum(0);
  ymPh  = momentum(1);
  zmPh= momentum(2);

  EPh=Energy;
  wPh=weight;
  PhaseSp-> Fill();
}

/*void RunAction:: FillHistoPhaseSpace2(G4ThreeVector position, G4ThreeVector momentum,G4double Energy, G4double weight)
{

  xPh2  = position(0);
  yPh2  = position(1);
  zPh2  = position(2);
  xmPh2 = momentum(0);
  ymPh2 = momentum(1);
  zmPh2 = momentum(2);

  EPh2  = Energy;
  wPh2=weight;
  PhaseSp2-> Fill();
}*/
