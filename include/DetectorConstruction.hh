#ifndef DetectorConstruction_H
#define DetectorConstruction_H 1

class G4LogicalVolume;
class G4VPhysicalVolume;

#include "G4VUserDetectorConstruction.hh"
#include "DetectorConstruction.hh"

#include "G4Material.hh"
#include "G4Box.hh"

#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"

#include "G4VisAttributes.hh"


#include "G4LogicalVolume.hh"
class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:

    DetectorConstruction();
    ~DetectorConstruction();

    G4VPhysicalVolume* Construct();

  private:
  G4Material* theMaterial;


   // Logical volumes
    //
    G4LogicalVolume* World_log;


    // Physical volumes
    //
    G4VPhysicalVolume* World_phys;

    G4LogicalVolume *XoftP00; // GDML
    G4LogicalVolume *XoftP01; // GDML
    G4LogicalVolume *XoftP02; // GDML
    G4LogicalVolume *XoftP03; // GDML
    G4LogicalVolume *XoftP04; // GDML
    G4LogicalVolume *XoftP05; // GDML
    G4LogicalVolume *XoftP06; // GDML
    G4LogicalVolume *XoftP07; // GDML
    G4LogicalVolume *XoftP08; // GDML
    G4LogicalVolume *XoftP09; // GDML
    G4LogicalVolume *XoftP10; // GDML
    G4LogicalVolume *XoftP11; // GDML
    G4LogicalVolume *XoftP12; // GDML
    G4LogicalVolume *XoftP13; // GDML
    G4LogicalVolume *XoftP14; // GDML
    G4LogicalVolume *XoftP15; // GDML
    G4LogicalVolume *XoftP16; // GDML
    G4LogicalVolume *XoftP17; // GDML
    G4LogicalVolume *XoftP18; // GDML
    G4LogicalVolume *XoftP19; // GDML
    G4LogicalVolume *XoftP20; // GDML


};

#endif
