# xRay

X-Ray tube starting from incident electrons

# From Shirin

This is the first part where e- are the primary and x-rays are scored in a phasespace file. The x-rays are scored in the Stepping action in a scoring volume (a thin box  (a plane)) defined in detector construction, just below Mo filter. The x-rays are scored in a root tree, the file is filled and stored in the runAction. 

By the way, define vacuum as G4_Galactica:
    `G4Material* vacuum = man->FindOrBuildMaterial("G4_Galactic");`
