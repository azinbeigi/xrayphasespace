
#include "DetectorConstruction.hh"
#include "G4UImanager.hh"
#include "G4Box.hh"
#include "G4Material.hh"
#include "G4LogicalVolume.hh"
#include "G4Tubs.hh"
#include "G4Trap.hh"
#include "G4Cons.hh"
#include "G4Sphere.hh"
#include "G4CutTubs.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "Randomize.hh"
#include "G4RandomDirection.hh"
#include "G4NistManager.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4UserLimits.hh"
#include "G4Ellipsoid.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"

// GDML Parser
#include "G4GDMLParser.hh"

DetectorConstruction::DetectorConstruction()
{

}

DetectorConstruction::~DetectorConstruction()
{
}

G4VPhysicalVolume* DetectorConstruction::Construct()
{

//////// START OF MATERIAL DEFINITIONS //////////////
  G4NistManager* man = G4NistManager::Instance();
  G4Element*  H  = man->FindOrBuildElement("H");
  G4Element*  Fe = man->FindOrBuildElement("Fe");
  G4Element*  Cr =man->FindOrBuildElement("Cr");
  G4Element*  Ni = man->FindOrBuildElement("Ni");
  G4Element*  Mn = man->FindOrBuildElement("Mn");
  G4Element*  Si = man->FindOrBuildElement("Si");
  G4Element*  C  = man->FindOrBuildElement("C");
  G4Element*  O  = man->FindOrBuildElement("O");
  G4Element*  Al  = man->FindOrBuildElement("Al");
  G4Element*  P  = man->FindOrBuildElement("P");
  //G4Element*  Ca  = man->FindOrBuildElement("Ca");
  G4Element* F = man->FindOrBuildElement("F");
  G4Element* W = man->FindOrBuildElement("W");
  G4Element* Ti = man->FindOrBuildElement("Ti");
  G4Element* Ag = man->FindOrBuildElement("Ag");
  G4Element* Cu = man->FindOrBuildElement("Cu");
  G4Element* In = man->FindOrBuildElement("In");
  G4Element* Co = man->FindOrBuildElement("Co");
  G4Element* Cl = man->FindOrBuildElement("Cl");
  G4Element* N = man->FindOrBuildElement("N");
  G4Element* Y = man->FindOrBuildElement("Y");
  //G4Element* Mn = man->FindOrBuildElement("Mn");
  G4Element* S = man->FindOrBuildElement("S");
  G4Element* Mo = man->FindOrBuildElement("Mo");

  G4Material* vacuum = man->FindOrBuildMaterial("G4_Galactic");
  G4Material* air = man->FindOrBuildMaterial("G4_AIR");
  
   G4Material* MatP00 =  new G4Material("MatP00", 10.1 *g/cm3, 2);
   MatP00 -> AddElement( W  , 0.9 );
   MatP00 -> AddElement( Ti , 0.1 );

   //01 - 60019146.gdml = Water  (CoolantCopperPort)
   //G4Material* MatP01 = man->FindOrBuildMaterial("G4_WATER");
    
   G4Material* MatP01 = new G4Material("MatP01",47, 107.868*g/
mole, 5.0*g/cm3);
   
   //G4Material* MatP01 -> FindOrBuildElement("Ag"); //Ash has the density of pure silver matterial as 5 g/c how is this possible?

   //02 - 72008048.gdml = Polyolefin
   G4Material* MatP02 =  new G4Material("MatP02", 1.0 *g/cm3, 2);
   MatP02 -> AddElement( H  , 2);
   MatP02 -> AddElement( C  , 1);

   //03 - 600185_LargerDiameter44.gdml = TEFLON
   G4Material* MatP03 =  man->FindOrBuildMaterial("G4_TEFLON");

   //04 - 60028542.gdml = Silicone
   G4Material* MatP04 =  new G4Material("MatP04", 1.1 *g/cm3, 5);
   MatP04 -> AddElement( H  , 500 );
   MatP04 -> AddElement( C  , 278 );
   MatP04 -> AddElement( Cl , 111 );
   MatP04 -> AddElement( N  , 55);
   MatP04 -> AddElement( Si , 56);

   //05 - Vacuum63.gdml = Vacuum
   G4Material* MatP05 = man->FindOrBuildMaterial("G4_Galactic");

   //06 - 600285_LargerDiameter45.gdml = Silicone
   G4Material* MatP06 =  new G4Material("MatP06", 1.1 *g/cm3, 5);
   MatP06 -> AddElement( H  , 500 );
   MatP06 -> AddElement( C  , 278 );
   MatP06 -> AddElement( Cl , 111 );
   MatP06 -> AddElement( N  , 55 );
   MatP06 -> AddElement( Si , 56 );

   //07 - 600055_.00434.gdml = WasherBraze
   G4Material* MatP07 =  new G4Material("MatP07", 9.8 *g/cm3, 3);
   MatP07 -> AddElement( Ag  , 6300 );
   MatP07 -> AddElement( Cu  , 3525 );
   MatP07 -> AddElement( Ti  , 175 );

   //08 - Coolant62.gdml = WATER
   G4Material* MatP08 = man->FindOrBuildMaterial("G4_WATER");

   //09 - 60018118.gdml = Alumina
   G4Material* MatP09=  new G4Material("MatP09", 3.96 *g/cm3, 2);
   MatP09 -> AddElement( Al , 4 );
   MatP09 -> AddElement( O  , 6 );

   //10- 720665_720004(Trimmed_720099)50.gdml = HDPE
   G4Material* MatP10=  new G4Material("MatP10", 0.95 *g/cm3, 2);
   MatP10 -> AddElement( H , 4 );
   MatP10 -> AddElement( C , 2 );

   //11 - 60000730.gdml = AluminumNitride w/ <5% Y2O3 (I'm using 5%)
   G4Material* AlN=  new G4Material("AlN", 3.26 *g/cm3, 2);
   AlN -> AddElement( Al , 1 );
   AlN -> AddElement( N  , 1 );
   G4Material* Y2O3=  new G4Material("Y2O3", 5.01 *g/cm3, 2);
   Y2O3 -> AddElement( Y , 2 );
   Y2O3 -> AddElement( O , 3 );
   G4double percY2O3    = 0.05; // percentage of Y2O3 in the anode subtrate mixture
   G4double densMixture = 3.26*(1-percY2O3) + 5.01*percY2O3;
   G4Material* MatP11=  new G4Material("MatP11", densMixture *g/cm3, 2);
   MatP11 -> AddMaterial( AlN  , 1-percY2O3 );
   MatP11 -> AddMaterial( Y2O3 , percY2O3 );

   //12 - 600061_600061-0335.gdml = WasherBrazeIncusil
   G4Material* MatP12=  new G4Material("MatP12", 9.7 *g/cm3, 4);
   MatP12 -> AddElement( Ag , 0.5875 );
   MatP12 -> AddElement( Cu , 0.2725 );
   MatP12 -> AddElement( In , 0.125 );
   MatP12 -> AddElement( Ti , 0.015 );

   // 13 - 600144_Shrunk39.gdml Pure Ag
   //w/ 25um Silver layer according to Xoft but this component is thinner than that so ignoring Silver
   //G4Material* Polyester = new G4Material("Polyester",1.38 *g/cm3, 3);
   //Polyester -> AddElement( C , 10 );
  // Polyester -> AddElement( H ,  8 );
   //Polyester -> AddElement( O ,  4 );
  

   //G4Material* Ag = man->FindOrBuildMaterial("G4_Ag"); // 10.49 g/cm3
   //G4double fracAg = 0.1;
  // G4double densityAgMix = 1.38*(1-fracAg) + fracAg*10.49;
   //G4Material* MatP13 = new G4Material("MatP13",densityAgMix *g/cm3, 2);
  // MatP13 -> AddMaterial( Polyester , 1-fracAg );
  G4Material* MatP13 =new G4Material ("MatP13", 10.49 * g/cm3, 1);
  MatP13 -> AddElement( Ag ,1 );

   //14 - 600654_shrunk245.gdml  = Teflon
   G4Material* MatP14 = man->FindOrBuildMaterial("G4_TEFLON");

   //15 - 60000932.gdml = Kovar
   G4Material* MatP15=  new G4Material("MatP15", 8.36 *g/cm3, 5);
   MatP15 -> AddElement( Fe , 0.535 );
   MatP15 -> AddElement( Ni , 0.290 );
   MatP15 -> AddElement( Co , 0.170 );
   MatP15 -> AddElement( Mn , 0.003 );
   MatP15 -> AddElement( Si , 0.002 );

   //16 - 600285_Fillet41.gdml = Silicone
   G4Material* MatP16 =  new G4Material("MatP16", 1.1 *g/cm3, 5);
   MatP16 -> AddElement( H  , 500 );
   MatP16 -> AddElement( C  , 278 );
   MatP16 -> AddElement( Cl , 111 );
   MatP16 -> AddElement( N  , 55 );
   MatP16 -> AddElement(Si , 56 );

   //17 - 720328_seg52.gdml = HDPE
   G4Material* MatP17=  new G4Material("MatP17", 0.95 *g/cm3, 2);
   MatP17 -> AddElement( H , 4 );
   MatP17 -> AddElement( C , 2 );
  //18 - 60027320.gdml = Cr2O3
  G4Material* MatP18= new G4Material("MatP18",5.22 *g/cm3, 2);
   MatP18 -> AddElement (Cr, 2);
   MatP18 -> AddElement (O, 3);
  //19 - MP-0021_TiW19.gdml = TiW
  G4Material* MatP19= new G4Material("MatP19", 10.1 *g/cm3, 2);
   MatP19 -> AddElement (Ti, 0.1);
   MatP19 -> AddElement (W, 0.9);


  //useful angles
  G4double startPhi = 0.;
  G4double spanPhi = 2.* CLHEP::pi;
// Define World
  G4Box* World = new G4Box("World", 180.0*CLHEP::cm, 180.0*CLHEP::cm, 180.0*CLHEP::cm);
 // Change material of the world to air
  World_log = new G4LogicalVolume(World, air, "World log", 0, 0, 0);
  World_phys = new G4PVPlacement(0, G4ThreeVector(0,0,0), "World_phys", World_log, 0, false, 0);

  
  G4RotationMatrix* rotationMatrix = new G4RotationMatrix();
  G4RotationMatrix* rotationMatrix_app = new G4RotationMatrix ();
  rotationMatrix->rotateX(-90.*deg);
  rotationMatrix_app->rotateZ(-25. *deg);

  //G4String gdmlPath ="/home/shirin/TPS/Xoft/GDML_Files/Volumes_FixedErrors/"; //Update
  G4String gdmlPath="/home/azinb/projects/def-senger/azinb/GDML_Files_Xoft/Volumes_FixedErrors_cluster/";
 // G4String gdmlPath_app="/home/shirin/TPS/Xoft/GDML_Files/AzinFiles/Applicator/Volumes/"; //Update
  std::cout << std::endl << "Loading GDML..."<< std::endl ;
   
  
  G4Tubs* score1= new G4Tubs("score1",2.75 *CLHEP::mm,3.75 *CLHEP::mm, 8 *CLHEP::mm, startPhi, spanPhi);
  //G4LogicalVolume* score1_log = new G4LogicalVolume(score1, air, "score1_log", 0, 0, 0);
  //G4PVPlacement* score1_phys = new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,-4.10 *CLHEP::mm),"score1_phys", score1_log, World_phys, false, 0);

  G4Tubs* score2= new G4Tubs("score2",0 *CLHEP::mm,2.75 *CLHEP::mm, 0.0005 *CLHEP::mm, startPhi, spanPhi);
 // G4LogicalVolume* score2_log = new G4LogicalVolume(score2, air, "score2_log", 0, 0, 0);
  //G4PVPlacement* score2_phys = new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,-12.1005 *CLHEP::mm),"score2_phys", score2_log, World_phys, false, 0);

  G4Tubs* score3= new G4Tubs("score3",0 *CLHEP::mm,2.75 *CLHEP::mm, 0.0005 *CLHEP::mm, startPhi, spanPhi);
  //G4LogicalVolume* score3_log = new G4LogicalVolume(score3, air, "score3_log", 0, 0, 0);
  //G4PVPlacement* score3_phys = new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0, 3.9005 *CLHEP::mm),"score3_phys", score3_log, World_phys, false, 0);
  
   G4UnionSolid* scoreunion1= new G4UnionSolid ("scoreunion1", score1, score2, 0, G4ThreeVector(0.0,0.0,8.0005));
   G4UnionSolid* scoreunion= new G4UnionSolid("scoreunion", scoreunion1, score3,0, G4ThreeVector(0.0,0.0,-8.0005));

     //G4LogicalVolume* scoring_world_log = new G4LogicalVolume(scoring_world, air, "scoring_world_log", 0, 0, 0);
   // G4PVPlacement* scoring_world_phys = new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0, 0.0),"scoring_world_log", scoring_world_log, World_phys, false, 0);
   
    G4LogicalVolume* scoreunion_log= new G4LogicalVolume(scoreunion, air, "scoreunion_log",0,0,0);
    G4VPhysicalVolume* scoreunion_phys = new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,-3.5 *CLHEP::mm),"scoreunion_phys",scoreunion_log, World_phys, false, 0);
  
  	//G4Tubs* scoring_world = new G4Tubs ( "scoring_world", 5 *CLHEP::mm, 170 *CLHEP::cm, 170 *CLHEP::cm, startPhi, spanPhi );
    //G4LogicalVolume* scoring_world_log = new G4LogicalVolume(scoring_world, air, "scoring_world_log", 0, 0, 0);
    //G4VPhysicalVolume* scoring_world_phys = new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0, 0.0),"scoring_world_phys", scoring_world_log, World_phys, false, 0);

    G4GDMLParser *gdmlPointer00 = new G4GDMLParser();
    gdmlPointer00->Read(gdmlPath + "600015TrgtCoat31.gdml");
    XoftP00 = gdmlPointer00->GetVolume("P600015TrgtCoat31");
    XoftP00->SetMaterial(MatP00);
    std::cout << "XoftP00 600015TrgtCoat31.gdml Mass= " << XoftP00->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att00 = new G4VisAttributes();
    gdml_att00->SetColor(0.5138915057879534, 0.6521741907719819, 0.14722219213167453); // Color
    XoftP00->SetVisAttributes(gdml_att00);

    G4GDMLParser *gdmlPointer01 = new G4GDMLParser();
    gdmlPointer01->Read(gdmlPath + "60019146.gdml");
    XoftP01 = gdmlPointer01->GetVolume("P60019146");
    XoftP01->SetMaterial(MatP01);
    std::cout << "XoftP01 60019146.gdml Mass= " << XoftP01->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att01 = new G4VisAttributes();
    gdml_att01->SetColor(0.7955800610252489, 0.5775277227873434, 0.5373667418480761); // Color
    XoftP01->SetVisAttributes(gdml_att01);

    G4GDMLParser *gdmlPointer02 = new G4GDMLParser();
    gdmlPointer02->Read(gdmlPath + "72008048.gdml");
    XoftP02 = gdmlPointer02->GetVolume("P72008048");
    XoftP02->SetMaterial(MatP02);
    std::cout << "XoftP02 72008048.gdml Mass=" << XoftP02->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att02 = new G4VisAttributes();
    gdml_att02->SetColor(0.8626174418674597, 0.9393434712618384, 0.9013061693638837); // Color
    XoftP02->SetVisAttributes(gdml_att02);

    G4GDMLParser *gdmlPointer03 = new G4GDMLParser();
    gdmlPointer03->Read(gdmlPath + "600185_LargerDiameter43.gdml");
    XoftP03 = gdmlPointer03->GetVolume("P600185LargerDiameter43");
    XoftP03->SetMaterial(MatP03);
    std::cout << "XoftP03 600185_LargerDiameter43.gdml Mass=" << XoftP03->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att03 = new G4VisAttributes();
    gdml_att03->SetColor(0.27448665656596294, 0.05173961154147133, 0.8920846858902238); // Color
    XoftP03->SetVisAttributes(gdml_att03);

    G4GDMLParser *gdmlPointer04 = new G4GDMLParser();
    gdmlPointer04->Read(gdmlPath + "60028542.gdml");
    XoftP04 = gdmlPointer04->GetVolume("P60028542");
    XoftP04->SetMaterial(MatP04);
    std::cout << "XoftP04 60028542.gdml Mass=" << XoftP04->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att04 = new G4VisAttributes();
    gdml_att04->SetColor(0.555471671828471, 0.28864286278580464, 0.643012171861012); // Color
    XoftP04->SetVisAttributes(gdml_att04);

    G4GDMLParser *gdmlPointer05 = new G4GDMLParser();
    gdmlPointer05->Read(gdmlPath + "Vacuum62.gdml");
    XoftP05 = gdmlPointer05->GetVolume("PVacuum62");
    XoftP05->SetMaterial(MatP05);
    std::cout << "XoftP05 Vacuum62.gdml Mass=" << XoftP05->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att05 = new G4VisAttributes();
    gdml_att05->SetColor(0.8490719773940157, 0.3597697679540428, 0.9572123221431332); // Color
    XoftP05->SetVisAttributes(gdml_att05);

    G4GDMLParser *gdmlPointer06 = new G4GDMLParser();
    gdmlPointer06->Read(gdmlPath + "600285_LargerDiameter44.gdml");
    XoftP06 = gdmlPointer06->GetVolume("P600285LargerDiameter44");
    XoftP06->SetMaterial(MatP06);
    std::cout << "XoftP06 600285_LargerDiameter44.gdml Mass=" << XoftP06->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att06 = new G4VisAttributes();
    gdml_att06->SetColor(0.1957815351781681, 0.16854940496115234, 0.9990200990209994); // Color
    XoftP06->SetVisAttributes(gdml_att06);

    G4GDMLParser *gdmlPointer07 = new G4GDMLParser();
    gdmlPointer07->Read(gdmlPath + "600055_.00434.gdml");
    XoftP07 = gdmlPointer07->GetVolume("P600055.00434");
    XoftP07->SetMaterial(MatP07);
    std::cout << "XoftP07 600055_.00434.gdml Mass=" << XoftP07->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att07 = new G4VisAttributes();
    gdml_att07->SetColor(0.9200723687819379, 0.032998876262786414, 0.3051969349473366); // Color
    XoftP07->SetVisAttributes(gdml_att07);

    G4GDMLParser *gdmlPointer08 = new G4GDMLParser();
    gdmlPointer08->Read(gdmlPath + "Coolant261.gdml");
    XoftP08 = gdmlPointer08->GetVolume("PCoolant261");
    XoftP08->SetMaterial(MatP08);
    std::cout << "XoftP08 Coolant261.gdml Mass=" << XoftP08->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att08 = new G4VisAttributes();
    gdml_att08->SetColor(0.009908497478146927, 0.9789706557568008, 0.13213750341414765); // Color
    XoftP08->SetVisAttributes(gdml_att08);

    G4GDMLParser *gdmlPointer09 = new G4GDMLParser();
    gdmlPointer09->Read(gdmlPath + "60018118.gdml");
    XoftP09 = gdmlPointer09->GetVolume("P60018118");
    XoftP09->SetMaterial(MatP09);
    std::cout << "XoftP09 60018118.gdml Mass=" << XoftP09->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att09 = new G4VisAttributes();
    gdml_att09->SetColor(0.050458705283012883, 0.7884524463565055, 0.7520477116927973); // Color
    XoftP09->SetVisAttributes(gdml_att09);

    G4GDMLParser *gdmlPointer10 = new G4GDMLParser();
    gdmlPointer10->Read(gdmlPath + "720665_72000450.gdml");
    XoftP10 = gdmlPointer10->GetVolume("P72066572000450");
    XoftP10->SetMaterial(MatP10);
    std::cout << "XoftP10 720665_72000450.gdml Mass =" << XoftP10->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att10 = new G4VisAttributes();
    gdml_att10->SetColor(0.0709613110931957, 0.4515692489543196, 0.5517844881090211); // Color
    XoftP10->SetVisAttributes(gdml_att10);

    G4GDMLParser *gdmlPointer11 = new G4GDMLParser();
    gdmlPointer11->Read(gdmlPath + "60000730.gdml");
    XoftP11 = gdmlPointer11->GetVolume("P60000730");
    XoftP11->SetMaterial(MatP11);
    std::cout << "XoftP11 60000730.gdml Mass =" << XoftP11->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att11 = new G4VisAttributes();
    gdml_att11->SetColor(0.4117474623724844, 0.5892760974751415, 0.4675457628761571); // Color
    XoftP11->SetVisAttributes(gdml_att11);

    G4GDMLParser *gdmlPointer12 = new G4GDMLParser();
    gdmlPointer12->Read(gdmlPath + "600061_600061-0335.gdml");
    XoftP12 = gdmlPointer12->GetVolume("P600061600061-0335");
    XoftP12->SetMaterial(MatP12);
    std::cout << "XoftP12 600061_600061-0335.gdml Mass =" << XoftP12->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att12 = new G4VisAttributes();
    gdml_att12->SetColor(0.9470570275700623, 0.11289215938463704, 0.010679214824020011); // Color
    XoftP12->SetVisAttributes(gdml_att12);

    G4GDMLParser *gdmlPointer13 = new G4GDMLParser();
    gdmlPointer13->Read(gdmlPath + "600144_Shrunk39.gdml");
    XoftP13 = gdmlPointer13->GetVolume("P600144Shrunk39");
    XoftP13->SetMaterial(MatP13);
    std::cout << "XoftP13 600144_Shrunk39.gdml Mass =" << XoftP13->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att13 = new G4VisAttributes();
    gdml_att13->SetColor(0.15096277070067654, 0.7286120357095256, 0.48086555969994726); // Color
    XoftP13->SetVisAttributes(gdml_att13);

    G4GDMLParser *gdmlPointer14 = new G4GDMLParser();
    gdmlPointer14->Read(gdmlPath + "600654_shrunk245.gdml");
    XoftP14 = gdmlPointer14->GetVolume("P600654shrunk245");
    XoftP14->SetMaterial(MatP14);
    std::cout << "XoftP14 600654_shrunk245.gdml Mass =" << XoftP14->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att14 = new G4VisAttributes();
    gdml_att14->SetColor(0.9560429116016341, 0.029054517545815894, 0.9793159359354348); // Color
    XoftP14->SetVisAttributes(gdml_att14);

    G4GDMLParser *gdmlPointer15 = new G4GDMLParser();
    gdmlPointer15->Read(gdmlPath + "60000932.gdml");
    XoftP15 = gdmlPointer15->GetVolume("P60000932");
    XoftP15->SetMaterial(MatP15);
    std::cout << "XoftP15 60000932.gdml Mass =" << XoftP15->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att15 = new G4VisAttributes();
    gdml_att15->SetColor(0.38361019852505485, 0.9132891695667787, 0.7481269099090913); // Color
    XoftP15->SetVisAttributes(gdml_att15);

    G4GDMLParser *gdmlPointer16 = new G4GDMLParser();
    gdmlPointer16->Read(gdmlPath + "600285_Fillet41.gdml");
    XoftP16 = gdmlPointer16->GetVolume("P600285Fillet41");
    XoftP16->SetMaterial(MatP16);
    std::cout << "XoftP16 600285_Fillet41.gdml Mass =" << XoftP16->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att16 = new G4VisAttributes();
    gdml_att16->SetColor(0.07897693594799438, 0.3744001795812929, 0.8767418645782885); // Color
    XoftP16->SetVisAttributes(gdml_att16);

    G4GDMLParser *gdmlPointer17 = new G4GDMLParser();
    gdmlPointer17->Read(gdmlPath + "720328_seg52.gdml"); // OUTER JACKET OF XOFT
    XoftP17 = gdmlPointer17->GetVolume("P720328seg52");
    XoftP17->SetMaterial(MatP17);
    std::cout << "XoftP17 720328_seg52.gdml Mass =" << XoftP17->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att17 = new G4VisAttributes();
    gdml_att17->SetColor(0.03965432547074221, 0.021125604663826736, 0.43426683800092025); // Color
    XoftP17->SetVisAttributes(gdml_att17);

    G4GDMLParser *gdmlPointer18 = new G4GDMLParser();
    gdmlPointer18->Read(gdmlPath + "60027320.gdml");
    XoftP18 = gdmlPointer18->GetVolume("P60027320");
    XoftP18->SetMaterial(MatP18);
    std::cout << "XoftP18 60027320.gdml Mass= " << XoftP18->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att18 = new G4VisAttributes();
    gdml_att18->SetColor(0.6138915057879534, 0.8521741907719819, 0.94722219213167453); // Color
    XoftP18->SetVisAttributes(gdml_att18);

    G4GDMLParser *gdmlPointer19 = new G4GDMLParser();
    gdmlPointer19->Read(gdmlPath + "MP-0021_TiW19.gdml");
    XoftP19 = gdmlPointer19->GetVolume("PMP-0021TiW19");
    XoftP19->SetMaterial(MatP19);
    std::cout << "XoftP19 MP-0021_TiW19.gdml Mass= " << XoftP19->GetMass() / g << " g" << std::endl;
    // Visualization
    G4VisAttributes *gdml_att19 = new G4VisAttributes();
    gdml_att19->SetColor(0.8138915057879534, 0.7521741907719819, 0.24722219213167453); // Color
    XoftP19->SetVisAttributes(gdml_att19);



  // SCORING REGION
  // To Match NIST MEASUREMENTS, also scoring at 178cm (partial sphere, we can filter out which sub regions to use for spectra in root later)
  G4Sphere* ScoringV2 = new G4Sphere("ScoringV2", 178 *CLHEP::cm, 178.0001 *CLHEP::cm, startPhi, spanPhi, 0, spanPhi/2.0 - 0.523599); // 60 deg openning in back (-150 to 150 deg like in rivard paper)
  G4LogicalVolume* ScoringV2_Logical = new G4LogicalVolume(ScoringV2, air, "ScoringV2_Logical");
  G4VPhysicalVolume* ScoringV2_P = new G4PVPlacement(0,G4ThreeVector(0,0,0),"ScoringV2_P", ScoringV2_Logical,World_phys,false,0);
  G4VisAttributes* ScoringV2_Att = new G4VisAttributes();
  ScoringV2_Att->SetColor(0.5, 0.5, 0.5);
  ScoringV2_Att->SetVisibility(true);
  ScoringV2_Att->SetForceSolid(false);
  ScoringV2_Logical->SetVisAttributes(ScoringV2_Att);



  // Placing XOFT Components
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP00", XoftP00, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP01", XoftP01, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP02", XoftP02, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP03", XoftP03, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP04", XoftP04, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP05", XoftP05, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP06", XoftP06, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP07", XoftP07, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP08", XoftP08, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP09", XoftP09, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP10", XoftP10, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP11", XoftP11, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP12", XoftP12, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP13", XoftP13, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP14", XoftP14, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP15", XoftP15, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP16", XoftP16, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP17", XoftP17, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP18", XoftP18, World_phys, false, 0);
  new G4PVPlacement(rotationMatrix_app, G4ThreeVector(0.0,0.0,0.0),"XoftP19", XoftP19, World_phys, false, 0);

G4Region *bremregion = new G4Region("bremregion");
bremregion -> AddRootLogicalVolume(XoftP00);
bremregion -> AddRootLogicalVolume(XoftP11);




  return World_phys;

}

   

