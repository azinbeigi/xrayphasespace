#include "PhysicsListMessenger.hh"

#include "PhysicsList.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"



PhysicsListMessenger::PhysicsListMessenger(PhysicsList* pPhys)
:G4UImessenger(),fPhysicsList(pPhys)
{
  fPhysDir = new G4UIdirectory("/xRay/phys/");
  fPhysDir->SetGuidance("physics list commands");

  fListCmd = new G4UIcmdWithAString("/xRay/phys/addPhysics",this);  
  fListCmd->SetGuidance("Add modula physics list.");
  fListCmd->SetParameterName("PList",false);
  fListCmd->AvailableForStates(G4State_PreInit);
  fListCmd->SetToBeBroadcasted(false);      
}



PhysicsListMessenger::~PhysicsListMessenger()
{
  delete fListCmd;
  delete fPhysDir;
}



void PhysicsListMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{ 
  if( command == fListCmd )
   { fPhysicsList->AddPhysicsList(newValue);}
}


