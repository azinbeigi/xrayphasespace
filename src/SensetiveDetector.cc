
#include "SensetiveDetector.hh"
#include "DetectorHit.hh"
#include "G4Step.hh"
#include "G4HCofThisEvent.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"

SensetiveDetector::SensetiveDetector(G4String name):G4VSensitiveDetector(name)
{
  G4String Hcname;
  collectionName.insert(Hcname="SensetiveDetectorCollection");
}
SensetiveDetector::~SensetiveDetector()
{
  G4cout<<"SensetiveDetector() deleted"<<G4endl;

}

void SensetiveDetector::initialize(G4HCofThisEvent* HCE)
{
  Collection=new DetectorHitsCollection(SensitiveDetectorName,
					  collectionName[0]);
  static G4int HCID = -1;
  if(HCID<0)
    {
      HCID=G4SDManager::GetSDMpointer() -> GetCollectionID(collectionName[0]); 
    }
  HCE->AddHitsCollection(HCID,Collection);
}
G4bool SensetiveDetector::ProcessHits(G4Step* aStep, G4TouchableHistory* ROhist)
{
  //G4Track* aTrack = aStep -> GetTrack();
  G4double edep = aStep -> GetTotalEnergyDeposit();
  if(edep==0.)
      return false;
    
  DetectorHit* newHit = new DetectorHit();
  newHit -> SetEdep(edep);
  newHit -> SetPos(aStep -> GetPreStepPoint() -> GetPosition());
  return true;
}

void SensetiveDetector::EndOfEvent(G4HCofThisEvent* HCE)
{;}
void SensetiveDetector::clear()
{;}
void SensetiveDetector:: DrawAll()
{;}
void SensetiveDetector::PrintAll()
{;}






