using namespace std;

#include "StackingAction.hh"
#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4Track.hh"
#include "G4TrackStatus.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4ios.hh"
#include "G4VTouchable.hh"

StackingAction*  StackingAction::pinst=NULL;

StackingAction*  StackingAction::GetInstance()
{
  
  return pinst;
}

G4ClassificationOfNewTrack StackingAction::ClassifyNewTrack(const G4Track * aTrack)
{

  G4ClassificationOfNewTrack     classification = fUrgent;

  /*
  const G4VTouchable* touchable = aTrack->GetOriginTouchable();
  
  if( aTrack->GetCreatorProcess() != NULL)
    {
      if(aTrack->GetCreatorProcess()->GetProcessName() == "RadioactiveDecay")
	{
	  if(aTrack->GetDefinition()->GetParticleName()=="gamma")
	    classification = fKill;
	  else
	    classification = fUrgent;
	}
    }
  */
  return classification;  

  //if(aTrack->GetCreatorProcess()->GetProcessName() =="PenelopeBrem")
  //{
  //  if(aTrack->GetDefinition()->GetParticleName()=="gamma")
  //    {
  //      
  //      classification = fKill;
  //    }
  //  else
  //    {
  //      classification = fUrgent;
  //    }
  //}
  //if(aTrack->GetCreatorProcess()->GetProcessName() =="PenelopeIoni")
  //{
  //  if(aTrack->GetDefinition()->GetParticleName()=="gamma")
  //    {
  //      
  //      classification = fKill;
  //    }
  //  else
  //    {
  //classification = fUrgent;
  //    }
  //}
  //}      
  //return classification;
  
}

