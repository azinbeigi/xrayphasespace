#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UIsession.hh"
#include "G4UItcsh.hh"
#include "DetectorConstruction.hh"

#include "PhysicsList.hh"
#include "PrimaryGeneratorAction.hh"
#include "RunAction.hh"
#include "EventAction.hh"
#include "SteppingAction.hh"
#include "DetectorHit.hh"
#include "SensetiveDetector.hh"


#include "G4UIExecutive.hh"
#ifdef G4VIS_USE
#include "VisManager.hh"
#endif

#ifdef G4VIS_USE
 #include "G4VisExecutive.hh"
#endif

#include "StackingAction.hh"





#include <cstdlib>
#include <sstream>
#include <iostream>
#include <string>
using namespace std;



int main(int argc, char** argv)
{


  G4String fileName;
  long int Tid=0;


  if(argc!=1)
    {
      fileName=argv[1];
      string timestring = argv[2];
      G4cout<<" timestring: "<<timestring<<G4endl;
      char * pEnd;

      Tid=strtol(argv[2],&pEnd,10);

      G4cout<<" Tid:  "<<Tid<<G4endl;
    }



  CLHEP::RanecuEngine *theRanGenerator = new CLHEP::RanecuEngine;
  theRanGenerator->setSeed(Tid);
  CLHEP::HepRandom::setTheEngine(theRanGenerator);
  G4cout <<Tid << G4endl;


  //----------------------------------------------------------
 // Construct the default run manager
  G4RunManager* runManager = new G4RunManager;
  //----------------------------------------------------------
  // set mandatory initialization classes

 DetectorConstruction* Detector = new DetectorConstruction();
 runManager->SetUserInitialization(Detector);

 PhysicsList* phys = new PhysicsList();
 phys->SetVerboseLevel(1);
 runManager->SetUserInitialization(phys);


  //---------------------------------------------------------
  // set user action classes
 PrimaryGeneratorAction* primary = new PrimaryGeneratorAction();
 runManager->SetUserAction(primary);

 RunAction* aRun = new RunAction(Detector,Tid);

 runManager->SetUserAction(aRun);
 runManager->SetUserAction( new StackingAction());
 EventAction*eventAction = new EventAction(aRun);
 runManager->SetUserAction(eventAction);
 runManager->SetUserAction(new SteppingAction(eventAction));
 //---------------------------------------------------------
 //Initialize G4 kernel
 //runManager->Initialize();
 //---------------------------------------------------------


G4UImanager* UI = G4UImanager::GetUIpointer();
 #ifdef G4VIS_USE
   G4VisManager* visManager = new G4VisExecutive;
   visManager->Initialize();
#endif
 if (argc!=1)
   {
     G4String command = "/control/execute ";
     G4String fileName = argv[1];
     UI->ApplyCommand(command+fileName);
   }
 else
    {
#ifdef G4UI_USE
      G4UIExecutive * ui = new G4UIExecutive(argc,argv);
      ui->SessionStart();
      delete ui;
#endif
    }



 G4cout<<"Slutet:  Main"<<G4endl;

 // job termination
#ifdef G4VIS_USE
 delete visManager;
#endif
 delete runManager;

 // G4cout<<"All deleting done"<<G4endl;




   return 0;
}
