
#ifndef DetectorHit_h
#define DetectorHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"

class DetectorHit : public G4VHit
{
public:
  DetectorHit();
  ~DetectorHit();
  DetectorHit(const DetectorHit &right);
  const DetectorHit & operator=(const DetectorHit &right);
  int operator==(const DetectorHit &right) const;
  inline void* operator new(size_t);
  inline void  operator delete(void* aHit);
 
  void Draw();
  void Print();
private:
  G4double edep;
  G4ThreeVector pos;
public:
  inline void SetEdep(G4double de)
  {
    edep=de; 
  }
  inline G4double GetEdep(G4double de)
  {
    return edep; 
  } 
  inline G4ThreeVector SetPos(G4ThreeVector xyz)
  {
    pos=xyz;
    return pos;
  } 
};
typedef G4THitsCollection<DetectorHit> DetectorHitsCollection;
extern G4Allocator<DetectorHit>DetectorHitAllocator;

inline void* DetectorHit::operator new(size_t)
{
  void *aHit;
  aHit=(void*) DetectorHitAllocator.MallocSingle();
  return aHit;
}
inline void DetectorHit::operator delete(void *aHit)
{
  DetectorHitAllocator.FreeSingle((DetectorHit*) aHit);
}

#endif








