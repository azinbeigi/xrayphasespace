#ifndef PrimaryGeneratorAction_h
#define PrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"

class G4ParticleGun;
class G4Event;


class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
public:
  PrimaryGeneratorAction();    
  virtual ~PrimaryGeneratorAction();
  //To use GPS comment "virtual void GeneratePrimaries(G4Event* event);", i.e. do not use this!
  virtual void GeneratePrimaries(G4Event* event);
  //To use GPS uncomment this:
  //void GeneratePrimaries(G4Event* anEvent);

private:
  //To use GPS comment the line bellow
  G4ParticleGun*  fParticleGun; // G4 particle gun
  //To use GPS uncomment bellow
  //G4GeneralParticleSource* particleGun;
};



#endif


