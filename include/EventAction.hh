#ifndef EventAction_h
#define EventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "RunAction.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

class EventAction : public G4UserEventAction
{
  public:
    EventAction(RunAction* aRun);
    ~EventAction();

  public:
    void BeginOfEventAction(const G4Event* anEvent);
    void EndOfEventAction(const G4Event* anEvent);

    void SetDrawFlag(G4String val)  {drawFlag = val;};


    void Set_X_Rays_Target(G4double);
    void Set_X_Rays_Filter1(G4double);
    void Set_X_Rays_Filter2(G4double);
    void Set_X_Rays_Filter3(G4double);
    void SetPos_E_Total(G4ThreeVector ,G4double );
    void SetPhaseSpace(G4ThreeVector, G4ThreeVector, G4double, G4double);
    void SetPhaseSpace2(G4ThreeVector, G4ThreeVector, G4double, G4double);
    //void SetTheDoseInTheBreast(G4double D);
   // G4double GetTheDoseInTheBreast();

private:

  RunAction* Run;

  G4String drawFlag;
  G4double SpectT,SpectB,SpectG;
  G4double TargetXE,Filter1XE,Filter2XE,Filter3XE;
  G4double wPhase, wPhase2; 
  G4double TotalEdep,dose,KinPhase, KinPhase2;
  G4ThreeVector posTotal,posPhase, momPhase;
  G4ThreeVector posTotal2,posPhase2, momPhase2;
};
#endif
