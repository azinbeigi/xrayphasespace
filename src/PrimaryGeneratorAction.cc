#include "PrimaryGeneratorAction.hh"

#include "G4RunManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"
#include "G4UserLimits.hh"




PrimaryGeneratorAction::PrimaryGeneratorAction()
 : G4VUserPrimaryGeneratorAction(),
   fParticleGun(0)
{
  G4int nofParticles = 1;
  fParticleGun = new G4ParticleGun(nofParticles);

  // default particle kinematic
  //
  G4ParticleDefinition* particleDefinition
    = G4ParticleTable::GetParticleTable()->FindParticle("e-");
  fParticleGun->SetParticleDefinition(particleDefinition);
  fParticleGun->SetParticlePosition(G4ThreeVector(0.0*CLHEP::mm, 0.0*CLHEP::mm, -4.0*CLHEP::mm));
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0,0,1));
  fParticleGun->SetParticleEnergy(50.*keV);
}



PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}



void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  // Circular Beam with Radius = 1.0 mm
  G4double rMax = 0.8 *mm; // Same as diameter of target from CAD design
  G4double x0   = rMax; //dummy value
  G4double y0   = rMax; //dummy value
  G4double z0   = -9.0 *mm;

  while (! (std::sqrt(x0*x0+y0*y0)<= rMax) )
  {
    x0 = CLHEP::RandFlat::shoot(-rMax,rMax);
    y0 = CLHEP::RandFlat::shoot(-rMax,rMax);
  }

  fParticleGun->SetParticlePosition(G4ThreeVector(x0, y0, z0));
  fParticleGun->SetParticleEnergy(50.*keV);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0,0,1));

  fParticleGun->GeneratePrimaryVertex(anEvent);
}

/*

#include "G4Event.hh"
#include "G4GeneralParticleSource.hh"

#include "PrimaryGeneratorAction.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction() {
  particleGun = new G4GeneralParticleSource();
}

PrimaryGeneratorAction::~PrimaryGeneratorAction() {
  delete particleGun;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent) {
  particleGun->GeneratePrimaryVertex(anEvent);
}

*/
