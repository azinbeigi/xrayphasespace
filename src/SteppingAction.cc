#include "G4ios.hh"
#include "SteppingAction.hh"
#include "G4Track.hh"
#include "globals.hh"
#include "G4SteppingManager.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"
#include "G4DynamicParticle.hh"
#include "G4Gamma.hh"
#include "G4VTouchable.hh"


SteppingAction::SteppingAction(EventAction* myEA):eventAction(myEA)
{ }



SteppingAction::~SteppingAction()
{ }



void SteppingAction::UserSteppingAction(const G4Step* aStep)
{
  G4Track* theTrack =  aStep->GetTrack();
  G4int StepNo = theTrack->GetCurrentStepNumber();
  G4double energydep = aStep -> GetTotalEnergyDeposit();
  G4StepPoint *thePostPoint = aStep->GetPostStepPoint();
  G4String partName = aStep-> GetTrack()->GetDefinition()->GetParticleName();
  G4double en = aStep->GetPreStepPoint()->GetKineticEnergy();
  G4double KinEnergy = theTrack->GetKineticEnergy();
  G4double weightparticle = theTrack ->GetWeight();
  const G4VTouchable* touchable = aStep->GetTrack()->GetOriginTouchable();

  // G4ThreeVector position = aStep->GetTrack()->GetPosition();
  /*
  G4double x = position.x();
  G4double y = position.y();
  G4double z = position.z();
  */

  // IF PHOTON EXITING TARGET Which Started in Target
 /*/ if( aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume()->GetName()=="XoftP00" &&
      aStep->GetPostStepPoint()->GetTouchableHandle()->GetVolume()->GetName()!="XoftP00" &&
      partName=="gamma")
          {
            eventAction ->Set_X_Rays_Target(KinEnergy/CLHEP::keV);
          }
  // IF PHOTON EXITING ANODE and started in Anode... (Not Really useful)
  else if( aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume()->GetName()=="XoftP11" &&
           aStep->GetPostStepPoint()->GetTouchableHandle()->GetVolume()->GetName()!="XoftP11" &&
           partName=="gamma" )
          {
            eventAction ->Set_X_Rays_Filter1(KinEnergy/CLHEP::keV);
          }*/
  // IF PHOTON EXITING XOFT JacketScoringV2_P
  //**THIS DOESN"T SCORE PHOTONS EXITING OUT OF THE REAR since P17 isn't close on the back surface!***
  // Not sure if above statement is fully true
    if( aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume()->GetName()=="scoreunion_phys" &&
           aStep->GetPostStepPoint()->GetPhysicalVolume()->GetName()=="World_phys" &&
           partName=="gamma")
          {
            eventAction ->Set_X_Rays_Filter2(KinEnergy/CLHEP::keV);
            G4ThreeVector position = aStep->GetPostStepPoint()->GetPosition()/CLHEP::mm;
            G4ThreeVector momentum = aStep->GetPostStepPoint()->GetMomentum();
            G4double energy = aStep->GetPostStepPoint()->GetKineticEnergy()/CLHEP::keV;
            G4double weight = aStep->GetPostStepPoint()->GetWeight();
            eventAction->SetPhaseSpace(position,momentum,energy,weight);
          }
  // 178 cm away partial sphere (see detector construction for detailed comment)
  /*else if( aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume()->GetName()=="ScoringV2_P" &&
           partName=="gamma")
          {
            eventAction ->Set_X_Rays_Filter3(KinEnergy/CLHEP::keV);
            G4ThreeVector position = aStep->GetPostStepPoint()->GetPosition()/CLHEP::mm;
            G4ThreeVector momentum = aStep->GetPostStepPoint()->GetMomentum();
            G4double energy = aStep->GetPostStepPoint()->GetKineticEnergy()/CLHEP::keV;
            G4double weight = aStep->GetPostStepPoint()->GetWeight();
            eventAction->SetPhaseSpace2(position,momentum,energy,weight);
          }*/

}
